/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.core.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author
 */
@Entity
@SequenceGenerator(allocationSize = 1, initialValue = 1, name = "comanda_seq", sequenceName = "comanda_seq")
@NamedQueries(value = {
    @NamedQuery(name = "buscar.comanda.por.codigo.comanda", query = "SELECT c FROM Comanda c WHERE c.codComanda =:codComanda AND c.aberta = true")
})
public class Comanda implements Serializable{
    
    @Id
    @GeneratedValue(generator = "comanda_seq", strategy = GenerationType.SEQUENCE)
    private Long id;
    private String codComanda;
    private String mesa;
    private boolean aberta;
    
    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private List<ItemPedido> pedidos;

    public Comanda() {
        initList();
    }

    public Comanda(Long id) {
        initList();
        this.id = id;
    }

    public Comanda(String codComanda, String mesa, boolean aberta) {
        initList();
        this.codComanda = codComanda;
        this.mesa = mesa;
        this.aberta = aberta;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodComanda() {
        return codComanda;
    }

    public void setCodComanda(String codComanda) {
        this.codComanda = codComanda;
    }

    public String getMesa() {
        return mesa;
    }

    public void setMesa(String mesa) {
        this.mesa = mesa;
    }

    public boolean isAberta() {
        return aberta;
    }

    public void setAberta(boolean aberta) {
        this.aberta = aberta;
    }

    public List<ItemPedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<ItemPedido> pedidos) {
        this.pedidos = pedidos;
    }
    
    private void initList(){
        this.pedidos = new ArrayList<>();
    }
    
    public Double calcular(){
        Double valor = new Double("0");
        for(ItemPedido item : this.pedidos){
            valor += item.getQtde() * item.getProduto().getPreco();
        }
        return valor;
    }
}
