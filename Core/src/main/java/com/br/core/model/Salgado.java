/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.core.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *
 * @author
 */
@Entity
@DiscriminatorValue(value = "SALGADO")
public class Salgado extends Produto{
    
    public Salgado() {
        super();
    }

    public Salgado(Long id) {
        super(id);
    }
    
    public Salgado(String descricao, Double preco) {
        super(descricao, preco);
    }
    
    public Salgado(String descricao) {
        super(descricao, 6.0);
    }
    
}
