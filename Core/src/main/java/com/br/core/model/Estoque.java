/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.core.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author
 */
@Entity
@SequenceGenerator(allocationSize = 1, initialValue = 1, name = "esto_seq", sequenceName = "esto_seq")
public class Estoque implements Serializable{
    
    @Id
    @GeneratedValue(generator = "esto_seq", strategy = GenerationType.SEQUENCE)
    private Long id;
    private Integer total;
    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Produto produto;

    public Estoque() {
    }

    public Estoque(Long id) {
        this.id = id;
    }

    public Estoque(Integer total, Produto produto) {
        this.total = total;
        this.produto = produto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }
    
}
