/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.core.model;

import java.io.Serializable;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author
 */
@Entity
@SequenceGenerator(allocationSize = 1, initialValue = 1, name = "prod_seq", sequenceName = "prod_seq")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "IDENTIFICADOR", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue(value = "PRODUTO")
@NamedQueries(value = {
    @NamedQuery(name = "buscar.produto.por.descricao", query = "SELECT e.produto FROM Estoque e WHERE e.produto.descricao =:descricao"),
    @NamedQuery(name = "buscar.produto.em.estoque.por.descricao", query = "SELECT e.produto FROM Estoque e WHERE e.produto.descricao =:descricao AND e.total > 0"),
    @NamedQuery(name = "buscar.estoque.por.descricao.produto", query = "SELECT e FROM Estoque e WHERE e.produto.descricao =:descricao"),
    @NamedQuery(name = "baixa.estoque", query = "UPDATE Estoque e SET e.total =:total WHERE e.id =:idEstoque"),
    @NamedQuery(name = "buscar.item.pelo.codigo", query = "SELECT i FROM ItemPedido i WHERE i.id =:idPedido")
})
public class Produto implements Serializable {

    @Id
    @GeneratedValue(generator = "prod_seq", strategy = GenerationType.SEQUENCE)
    private Long id;
    private String descricao;
    private Double preco;

    public Produto() {
    }

    public Produto(Long id) {
        this.id = id;
    }

    public Produto(String descricao, Double preco) {
        this.descricao = descricao;
        this.preco = preco;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }
    
}
