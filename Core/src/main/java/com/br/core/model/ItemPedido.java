/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.core.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author
 */
@Entity
@SequenceGenerator(allocationSize = 1, initialValue = 1, name = "item_seq", sequenceName = "item_seq")
public class ItemPedido implements Serializable{
    
    @Id
    @GeneratedValue(generator = "item_seq", strategy = GenerationType.SEQUENCE)
    private Long id;
    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Produto produto;
    private Integer qtde;
    private boolean entregue;

    public ItemPedido() {
    }

    public ItemPedido(Long id) {
        this.id = id;
    }

    public ItemPedido(Integer qtde, boolean entregue) {
        this.qtde = qtde;
        this.entregue = entregue;
    }

    public ItemPedido(Produto produto, Integer qtde, boolean entregue) {
        this.produto = produto;
        this.qtde = qtde;
        this.entregue = entregue;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQtde() {
        return qtde;
    }

    public void setQtde(Integer qtde) {
        this.qtde = qtde;
    }

    public boolean isEntregue() {
        return entregue;
    }

    public void setEntregue(boolean entregue) {
        this.entregue = entregue;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }
    
}
