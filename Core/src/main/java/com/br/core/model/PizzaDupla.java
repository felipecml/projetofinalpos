/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.core.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *
 * @author
 */
@Entity
@DiscriminatorValue(value = "PIZZADUPLA")
public class PizzaDupla extends Produto {

    public PizzaDupla() {
    }

    public PizzaDupla(Long id) {
        super(id);
    }

    public PizzaDupla(String descricao, Double preco) {
        super(descricao, preco);
    }
    
    public PizzaDupla(String descricao) {
        super(descricao, 32.0);
    }
    
}
