/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.core.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *
 * @author
 */
@Entity
@DiscriminatorValue(value = "PIZZASIMPLES")
public class PizzaSimples extends Produto{

    public PizzaSimples() {
    }

    public PizzaSimples(Long id) {
        super(id);
    }

    public PizzaSimples(String descricao, Double preco) {
        super(descricao, preco);
    }
    
    public PizzaSimples(String descricao) {
        super(descricao, 28.0);
    }
    
}
