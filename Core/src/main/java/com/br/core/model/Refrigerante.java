/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.core.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *
 * @author
 */
@Entity
@DiscriminatorValue(value = "REFRIGERANTE")
public class Refrigerante extends Produto{

    public Refrigerante() {
    }

    public Refrigerante(Long id) {
        super(id);
    }
    
    public Refrigerante(String descricao) {
        super(descricao, 8.0);
    }
    
    public Refrigerante(String descricao, Double preco) {
        super(descricao, preco);
    }

}
