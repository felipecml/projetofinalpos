/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.core.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author
 */
@Entity
@SequenceGenerator(allocationSize = 1, initialValue = 1, name = "lanc_seq", sequenceName = "lanc_seq")
public class LancamentoCaixa implements Serializable{
    
    @Id
    @GeneratedValue(generator = "lanc_seq", strategy = GenerationType.SEQUENCE)
    private Long id;
    private Double valor;
    @OneToOne
    private Comanda comanda;    

    public LancamentoCaixa() {
    }

    public LancamentoCaixa(Long id) {
        this.id = id;
    }
    
    public LancamentoCaixa(Double valor, Comanda comanda) {
        this.valor = valor;
        this.comanda = comanda;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Comanda getComanda() {
        return comanda;
    }

    public void setComanda(Comanda comanda) {
        this.comanda = comanda;
    }
        
}
