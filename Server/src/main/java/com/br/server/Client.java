/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.server;

import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;

/**
 *
 * @author Rodrigo
 */
public class Client {

    public static void main(String[] args) throws IOException, JSONException {

        /*========================================================================================================================================*/

        //PARA O TESTE DE NEGÓCIO POSITIVO, CRIE UMA NOVA COMANDA E PARA O TESTE DE NEGÓCIO NEGATIVO TENTE ABRIR UMA
        //COMANDA NOVA PARA A MESMA MESA E MESMA COMANDA, É PARA EXIBIR UMA MENSAGEM INFORMANDO QUE JÁ EXISTE UMA COMANDA
        //ABERTA PARA AQUELA MESA.
        /* ABRIR COMANDA */
        //REPRESENTACAO
        //{ codMesa : "codigo da mesa", codComanda : "codigo da comanda" }

//        JSONObject json = new JSONObject();
//        json.put("codMesa", "024");
//        json.put("codComanda", "502");
//        JsonRepresentation jsonrep = new JsonRepresentation(json);
//        ClientResource resource = new ClientResource("http://localhost:9099/app/atendimento");
//        Representation rep = (Representation) resource.post(jsonrep);
//        System.out.println(rep.getText());

        /*========================================================================================================================================*/

        /* CADASTRAR PRODUTO */
        //REPRESENTACAO
        //{tipo : "tipo de produto", descricao : "descricao do produto", qtde : "quantidade"}
//        JSONObject json = new JSONObject();
//        json.put("tipo", "refrigerantes");
//        json.put("descricao", "Fanta");
//        json.put("qtde", "700");
//        JsonRepresentation jsonrep = new JsonRepresentation(json);
//        ClientResource resource = new ClientResource("http://localhost:9099/app/produto");
//        Representation rep = (Representation) resource.post(jsonrep);
//        System.out.println(rep.getText());

        /*========================================================================================================================================*/

        /* FAZER PEDIDO */
        //REPRESENTACAO
        //{tipo : "pedido de item ou entregar de pedido", descricao : "descricao do pedido",
        //codComada : "codigo da comanda", tipoProduto : "tipo do produto", qtde : "quantidade"}
//        JSONObject json = new JSONObject();
//
//        json.put("tipo", "pedido");
//        json.put("descricao", "Fanta");
//        json.put("codComanda", "502");
//        json.put("tipoProduto", "refri");
//        json.put("qtde", "30");
//
//        JsonRepresentation jsonrep = new JsonRepresentation(json);
//        ClientResource resource = new ClientResource("http://localhost:9099/app/pedidos");
//        Representation rep = (Representation) resource.put(jsonrep);
//        System.out.println(rep.getText());

        /*========================================================================================================================================*/

        /* ENTREGAR PEDIDO */
        //REPRESENTACAO
        //{tipo : "entregar pedido", codComanda : "codigo da comanda", codItem : "codigo do item de pedido"}
//        JSONObject json = new JSONObject();
//
//        json.put("tipo", "entrega");
//        json.put("codComanda", "502");
//        json.put("codItem", "11");
//       
//        JsonRepresentation jsonrep = new JsonRepresentation(json);
//        ClientResource resource = new ClientResource("http://localhost:9099/app/pedidos");
//        Representation rep = (Representation) resource.put(jsonrep);
//        System.out.println(rep.getText());

        /*========================================================================================================================================*/

        /* FAZER BAIXA EM ESTOQUE */
        //REPRESENTACAO
        //{descricao : "descricao do produto" , qtde : "quantidade"}
//        JSONObject json = new JSONObject();
//
//        json.put("descricao", "Fanta");
//        json.put("qtde", "30");
//
//        JsonRepresentation jsonrep = new JsonRepresentation(json);
//        ClientResource resource = new ClientResource("http://localhost:9099/app/estoque");
//        Representation rep = (Representation) resource.put(jsonrep);
//        System.out.println(rep.getText());

        /*========================================================================================================================================*/

        /* PAGAR COMANDA */
        //REPRESENTACAO
        //{codComanda : "codigo da comanda"}
//        JSONObject json = new JSONObject();
//
//        json.put("codComanda", "502");
//       
//        JsonRepresentation jsonrep = new JsonRepresentation(json);
//        ClientResource resource = new ClientResource("http://localhost:9099/app/financeiro");
//        Representation rep = (Representation) resource.post(jsonrep);
//        System.out.println(rep.getText());
    }
}