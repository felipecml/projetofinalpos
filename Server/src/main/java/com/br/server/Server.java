package com.br.server;

import br.com.resource.basico.CaixaResource;
import br.com.resource.basico.PedidoResource;
import br.com.resource.basico.ProdutoResource;
import br.com.resource.intermediario.ManagerResourceCaixa;
import br.com.resource.intermediario.ManagerResourceEstoque;
import br.com.resource.intermediario.ManagerResourcePedido;
import br.com.resource.manager.ComandaResource;
import br.com.resource.manager.EstoqueResource;
import br.com.resource.manager.FinancaResource;
import org.restlet.Application;
import org.restlet.Component;
import org.restlet.data.Protocol;
import org.restlet.routing.Router;

/**
 *
 * @author Rodrigo
 */
public class Server {

    public static void main(String[] args) throws Exception {
        Component component = new Component();
        component.getServers().add(Protocol.HTTP, 9099);
        component.getClients().add(Protocol.HTTP);

        /**
         * ROTEAMENTO DAS OPERAÇÕES
         */        
        Application app = new Application();
        Router router = new Router();
        
        /* ROTEANDO TODAS AS OPERAÇÕES BÁSICAS */
        //criar uma comanda POST
        router.attach("/comanda", PedidoResource.class);
        //recuperar item ou comanda GET
        router.attach("/recuperar/{tipo}/{cod}", PedidoResource.class);
        //realizar pedido PUT
        router.attach("/atualizar", PedidoResource.class);
        //cadastrar produto POST
        router.attach("/cadastro", ProdutoResource.class);
        //recuperar produto ou estoque GET
        router.attach("/buscar/{tipo}/{descricao}", ProdutoResource.class);
        //atualizar estoque
        router.attach("/repor", ProdutoResource.class);
        //lançamento no caixa POST
        router.attach("/lancamento", CaixaResource.class);
        
        /* ROTEANDO TODAS AS OPERAÇÕES INTERMEDIARIAS */
        //realizar um pedido ou uma entrega de algum item PUT
        router.attach("/pedido", ManagerResourcePedido.class);    
        //realizar o pagamento de uma comanda POST
        router.attach("/pagamento", ManagerResourceCaixa.class);         
        //realizar a baixa no estoque 
        router.attach("/baixa", ManagerResourceEstoque.class);         
        
        /* ROTEANDO TODAS AS OPERAÇÕES DE NEGOCIO*/
        //roteando serviço de baixa de estoque PUT
        router.attach("/estoque", EstoqueResource.class);
        //cadastrar produto POST
        router.attach("/produto", EstoqueResource.class);
        //realizar pedido e entregar item PUT
        router.attach("/pedidos", ComandaResource.class);
        //abrir comanda POST
        router.attach("/atendimento", ComandaResource.class);
        //realizar pagamento POST
        router.attach("/financeiro", FinancaResource.class);
        
        app.setInboundRoot(router);
        component.getDefaultHost().attach("/app", app);

        component.start();
    }
}
