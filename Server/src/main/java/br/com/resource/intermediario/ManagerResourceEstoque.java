/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.resource.intermediario;

import com.br.core.model.Estoque;
import com.google.gson.Gson;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

/**
 *
 * @author Felipe
 */
public class ManagerResourceEstoque extends ServerResource{
    
    private ClientResource clientResourceProduto;
    
    @Put
    public Representation baixaNoEstoque(Representation rep) {
        try {

            Representation result;
            JsonRepresentation jsonrep = new JsonRepresentation(rep);
            JSONObject json = jsonrep.getJsonObject();

            //recupera a representação
            String descricao = json.getString("descricao");
            int qtde = Integer.valueOf(json.getString("qtde"));

            this.clientResourceProduto = new ClientResource("http://localhost:9099/app/buscar/estoque/" + descricao);
            result = this.clientResourceProduto.get();//recupera o estoque
            Estoque e = new Gson().fromJson(result.getText(), Estoque.class);

            int novoValor = e.getTotal() - qtde;
            e.setTotal(novoValor);

            JsonRepresentation jsono = new JsonRepresentation(new Gson().toJson(e));
            this.clientResourceProduto = new ClientResource("http://localhost:9099/app/repor");
            result = this.clientResourceProduto.put(jsono);

            return new JsonRepresentation(result.getText());

        } catch (JSONException ex) {
            return new JsonRepresentation("ERRO: " + ex.getMessage());
        } catch (IOException ex) {
            return new JsonRepresentation("ERRO: " + ex.getMessage());
        }
    }
    
}
