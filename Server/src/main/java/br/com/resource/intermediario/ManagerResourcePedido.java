/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.resource.intermediario;

import com.br.core.model.Comanda;
import com.br.core.model.ItemPedido;
import com.br.core.model.PizzaDupla;
import com.br.core.model.PizzaSimples;
import com.br.core.model.Produto;
import com.google.gson.Gson;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

/**
 *
 * @author Rodrigo
 */
public class ManagerResourcePedido extends ServerResource {

    private ClientResource clientResourcePedido;
    private ClientResource clientResourceProduto;

    @Put
    public Representation gerenciarPedido(Representation rep) {
        try {

            Produto p;
            Representation result;
            Representation res;

            JsonRepresentation jsonrep = new JsonRepresentation(rep);
            JSONObject json = jsonrep.getJsonObject();

            //recupera a representação
            String tipo = json.getString("tipo");
            String codComanda = json.getString("codComanda");

            //pedido
            if (tipo.equalsIgnoreCase("pedido") || tipo.contains("ped")) {

                String tipoProduto = json.getString("tipoProduto");
                String descricao = json.getString("descricao");
                int qtde = Integer.valueOf(json.getString("qtde"));

                try {
                    
                    this.clientResourcePedido = new ClientResource("http://localhost:9099/app/recuperar/comanda/" + codComanda);

                    result = this.clientResourcePedido.get();//recupera a comanda
                    Comanda c = new Gson().fromJson(result.getText(), Comanda.class);

                    if (tipoProduto.equalsIgnoreCase("salgados") || tipoProduto.equalsIgnoreCase("refrigerantes") || tipoProduto.contains("sal") || tipoProduto.contains("ref")) {
                        this.clientResourceProduto = new ClientResource("http://localhost:9099/app/buscar/produto/" + descricao);
                        res = this.clientResourceProduto.get();//recupera o produto
                        p = new Gson().fromJson(res.getText(), Produto.class);
                    } else {
                        //pizza
                        if (tipoProduto.equalsIgnoreCase("simples") || tipoProduto.contains("sim")) {
                            //pizza simples
                            p = new PizzaSimples(descricao);
                        } else {
                            //pizza dupla
                            p = new PizzaDupla(descricao);
                        }
                    }

                    c.getPedidos().add(new ItemPedido(p, qtde, false));

                    JsonRepresentation jsono = new JsonRepresentation(new Gson().toJson(c));
                    this.clientResourcePedido = new ClientResource("http://localhost:9099/app/atualizar");
                    this.clientResourcePedido.put(jsono);
                    return new JsonRepresentation("Pedido realizado com sucesso!");
                    
                } catch (Exception e) {
                    e.printStackTrace();
                    return new JsonRepresentation("Não foi possivel realizar este pedido: " + e.getMessage());
                }
            }

            if (tipo.equalsIgnoreCase("entrega") || tipo.contains("ent")) {

                Long codItem = Long.valueOf(json.getString("codItem"));
                this.clientResourcePedido = new ClientResource("http://localhost:9099/app/recuperar/comanda/" + codComanda);
                result = this.clientResourcePedido.get();//recupera a comanda
                Comanda c = new Gson().fromJson(result.getText(), Comanda.class);

                for (ItemPedido i : c.getPedidos()) {
                    if (i.getId() == codItem) {
                        i.setEntregue(true);
                        break;
                    }
                }

                JsonRepresentation jsono = new JsonRepresentation(new Gson().toJson(c));
                this.clientResourcePedido = new ClientResource("http://localhost:9099/app/atualizar");
                this.clientResourcePedido.put(jsono);
                return new JsonRepresentation("Pedido entregue com sucesso!");

            }
            //entregar
            String retorno = null;

            return new JsonRepresentation(retorno);

        } catch (IOException | JSONException ex) {
            return new JsonRepresentation(ex.getMessage());
        }

    }

}
