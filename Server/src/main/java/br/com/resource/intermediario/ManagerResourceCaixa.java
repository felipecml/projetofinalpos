/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.resource.intermediario;

import com.br.core.model.Comanda;
import com.br.core.model.LancamentoCaixa;
import com.google.gson.Gson;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

/**
 *
 * @author Felipe
 */
public class ManagerResourceCaixa extends ServerResource{

    private ClientResource clientResourcePedido;
    
    @Post
    public Representation pagamento(Representation rep) {
        try {
            Representation result;

            JsonRepresentation jsonrep = new JsonRepresentation(rep);
            JSONObject json = jsonrep.getJsonObject();

            //recupera a representação
            String codComanda = json.getString("codComanda");

            //recupera a comanda
            this.clientResourcePedido = new ClientResource("http://localhost:9099/app/recuperar/comanda/" + codComanda);
            result = this.clientResourcePedido.get();//recupera a comanda
            Comanda c = new Gson().fromJson(result.getText(), Comanda.class);
            c.setAberta(false);

            //fecha a comanda
            JsonRepresentation jsono = new JsonRepresentation(new Gson().toJson(c));
            this.clientResourcePedido = new ClientResource("http://localhost:9099/app/atualizar");
            this.clientResourcePedido.put(jsono);

            //registra o lancamento
            LancamentoCaixa caixa = new LancamentoCaixa(c.calcular(), c);
            JsonRepresentation j = new JsonRepresentation(new Gson().toJson(caixa));
            ClientResource resource = new ClientResource("http://localhost:9099/app/lancamento");
            Representation r = (Representation) resource.post(j);

            return new JsonRepresentation(r.getText());
            
        } catch (IOException ex) {
            return new JsonRepresentation("Erro: " + ex.getMessage());
        } catch (JSONException ex) {
            return new JsonRepresentation("Erro: " + ex.getMessage());
        }
    }
    
}
