/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.resource.basico;

import com.br.business.execoes.ComandaException;
import com.br.business.pedido.PedidoBusiness;
import com.br.core.model.Comanda;
import com.br.core.model.ItemPedido;
import com.google.gson.Gson;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

/**
 *
 * @author Rodrigo
 */
public class PedidoResource extends ServerResource {

    private final PedidoBusiness pedidoManager = new PedidoBusiness();

    @Post
    public Representation criarComanda(Representation rep) {
        String retorno;
        try {
            JsonRepresentation jsonrep = new JsonRepresentation(rep);
            JSONObject json = jsonrep.getJsonObject();

            String codMesa = json.getString("codMesa");
            String codComanda = json.getString("codComanda");

            if (pedidoManager.abrirComanda(codMesa, codComanda)) {
                retorno = "Comanda criada com sucesso!";
            } else {
                retorno = "Não foi possivel criar a comanda.";
            }

        } catch (IOException | JSONException | ComandaException ex) {
            retorno = ex.getMessage();
        }
        return new JsonRepresentation(retorno);
    }

    @Get
    public Representation recuperar() {
        try {
            String cod = (String) getRequestAttributes().get("cod");
            String tipo = (String) getRequestAttributes().get("tipo");

            if (tipo.equalsIgnoreCase("comanda") || tipo.contains("com")) {
                Comanda c = this.pedidoManager.recuperarComanda(cod);
                return new JsonRepresentation(new Gson().toJson(c));
            }
            
            if (tipo.equalsIgnoreCase("item") || tipo.contains("ite")) {
                ItemPedido i = this.pedidoManager.recuperarItem(Long.valueOf(cod));
                return new JsonRepresentation(new Gson().toJson(i));
            }
            
            return new JsonRepresentation("Não foi informado o objeto que se deseja recuperar, por favor, escolha entre 'cod' e 'tipo'.");
         
        } catch (ComandaException  ex) {
            return new JsonRepresentation(ex.getMessage());
        }
        
    }

    @Put
    public Representation atualizar(Representation rep) {
        try {
            Comanda c = new Gson().fromJson(rep.getText(), Comanda.class);
            if (this.pedidoManager.atualizarComanda(c)) {
                return new JsonRepresentation("Comanda atualizada com sucesso!");
            }
            return new JsonRepresentation("Não foi possivel atualizar esta comanda.");
        } catch (IOException ex) {
            return new JsonRepresentation(ex.getMessage());
        }
    }

}