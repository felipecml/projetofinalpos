/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.resource.basico;

import com.br.business.execoes.ProdutoException;
import com.br.business.produto.ProdutoBusiness;
import com.br.core.model.Estoque;
import com.br.core.model.Produto;
import com.br.core.model.Refrigerante;
import com.br.core.model.Salgado;
import com.google.gson.Gson;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

/**
 *
 * @author Rodrigo
 */
public class ProdutoResource extends ServerResource {

    private final ProdutoBusiness produtoManager = new ProdutoBusiness();
    
    @Post
    public Representation cadastrar(Representation rep) {
        String retorno;
        boolean status;

        try {
            JsonRepresentation jsonrep = new JsonRepresentation(rep);
            JSONObject json = jsonrep.getJsonObject();

            String tipoDeProduto = json.getString("tipo");
            String descricao = json.getString("descricao");
            int qtde = json.getInt("qtde");

            if (tipoDeProduto.equalsIgnoreCase("refrigerante") || tipoDeProduto.contains("refr")) {
                Refrigerante refrigerante = new Refrigerante(descricao);
                status = produtoManager.cadastrarProduto(refrigerante, qtde);
                retorno = getMSGCadastro(status);
            } else if (tipoDeProduto.equalsIgnoreCase("salgados") || tipoDeProduto.contains("sal")) {
                Salgado salgado = new Salgado(descricao);
                status = produtoManager.cadastrarProduto(salgado, qtde);
                retorno = getMSGCadastro(status);
            } else {
                retorno = "Este tipo de produto não pode ser cadastrado. Por favor selecione: Refrigerante ou Salgados";
            }

        } catch (IOException | JSONException | ProdutoException ex) {
            retorno = ex.getMessage();
        }
        return new JsonRepresentation(retorno);
    }

    @Get
    /**
     * RECUPERA PRODUTO OU ESTOQUE
     */
    public Representation recuperar() {

        String tipo = (String) getRequestAttributes().get("tipo");
        String descricao = (String) getRequestAttributes().get("descricao");

        if (tipo.equalsIgnoreCase("produto") || tipo.contains("pro")) {
            Produto p = this.produtoManager.buscarProduto(descricao.replace("%20", " "));
            return new JsonRepresentation(new Gson().toJson(p));
        }
        if (tipo.equalsIgnoreCase("estoque") || tipo.contains("est")) {
            Estoque e = this.produtoManager.consultarEstoquePorProduto(descricao.replace("%20", " "));
            return new JsonRepresentation(new Gson().toJson(e));
        }

        return new JsonRepresentation("O tipo informado não corresponde a um Produto ou Estoque.");
    }

    @Put
    public Representation atualizar(Representation rep) {
        try {

            Estoque e = new Gson().fromJson(rep.getText(), Estoque.class);
            if (this.produtoManager.atualizar(e)) {
                return new JsonRepresentation("Estoque atualizado com sucesso!");
            }
            return new JsonRepresentation("Não foi possivel atualizar este estoque.");
        } catch (IOException ex) {
            return new JsonRepresentation("Não foi possivel recuperar este estoque. ERRO: " + ex.getMessage());
        }

    }

    private String getMSGCadastro(boolean status) {
        if (status) {
            return "Produto cadastrado com sucesso!";
        } else {
            return "Não foi possivel cadastrar este produto. Entre em contato com o administrador do sistema.";
        }
    }

    private String getMSGBaixaEstoque(boolean status) {
        if (status) {
            return "Estoque debitado com sucesso!";
        } else {
            return "Não foi possivel debitar.";
        }
    }
}
