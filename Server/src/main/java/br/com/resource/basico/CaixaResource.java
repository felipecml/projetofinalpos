/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.resource.basico;

import com.br.business.caixa.CaixaBusiness;
import com.br.business.execoes.CaixaException;
import com.br.core.model.LancamentoCaixa;
import com.google.gson.Gson;
import java.io.IOException;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

/**
 *
 * @author Rodrigo
 */
public class CaixaResource extends ServerResource {

    private final CaixaBusiness business = new CaixaBusiness();
            
    @Post
    public Representation cadastrar(Representation rep){
        try {
            LancamentoCaixa caixa = new Gson().fromJson(rep.getText(), LancamentoCaixa.class);
            if( this.business.lancamento(caixa) ){
                return new JsonRepresentation("Lançamento realizado com sucesso!");
            }
            return new JsonRepresentation("Não foi possivel realizar este lançamento.");
        } catch (IOException ex) {
            return new JsonRepresentation("Nao foi possivel gravar este lançamento. ERRO: " + ex.getMessage());
        } catch (CaixaException ex) {
            return new JsonRepresentation("Nao foi possivel gravar este lançamento. ERRO: " + ex.getMessage());
        }
    }
    
}
