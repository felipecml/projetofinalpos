/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.resource.manager;

import java.io.IOException;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

/**
 *
 * @author Felipe
 */
public class FinancaResource extends ServerResource{
    
    private ClientResource clientManagerResource;
    
    @Post
    public Representation pagarComanda(Representation rep){
        try {
            this.clientManagerResource = new ClientResource("http://localhost:9099/app/pagamento");
            Representation resp = this.clientManagerResource.post(rep);
            return new JsonRepresentation( resp.getText() );
        } catch (IOException ex) {
            return new JsonRepresentation( "ERRO: " + ex.getMessage());
        }
    }
    
}
