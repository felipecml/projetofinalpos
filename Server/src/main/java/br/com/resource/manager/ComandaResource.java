/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.resource.manager;

import java.io.IOException;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

/**
 *
 * @author Rodrigo
 */
public class ComandaResource extends ServerResource {

    private ClientResource clienteManagerResource;
    
    @Post
    public Representation abrirComanda(Representation rep) {
        try {
            this.clienteManagerResource = new ClientResource("http://localhost:9099/app/comanda");
            Representation resp = this.clienteManagerResource.post(rep);
            return new JsonRepresentation( resp.getText() );
        } catch (IOException ex) {
            return new JsonRepresentation( "ERRO: " + ex.getMessage());
        }
    }
    
    @Put
    public Representation realizarPedidoOuEntregarItem(Representation rep) {
      try {
            this.clienteManagerResource = new ClientResource("http://localhost:9099/app/pedido");
            Representation resp = this.clienteManagerResource.put(rep);
            return new JsonRepresentation( resp.getText() );
        } catch (IOException ex) {
            return new JsonRepresentation( "ERRO: " + ex.getMessage());
        }
    }
}
