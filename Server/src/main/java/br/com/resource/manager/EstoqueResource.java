/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.resource.manager;

import java.io.IOException;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

/**
 *
 * @author Felipe
 */
public class EstoqueResource extends ServerResource {

    private ClientResource clientResourceProduto;

    @Post
    public Representation cadastrarProduto(Representation rep) {
        try {
            this.clientResourceProduto = new ClientResource("http://localhost:9099/app/cadastro");
            Representation resp = this.clientResourceProduto.post(rep);
            return new JsonRepresentation( resp.getText() );
        } catch (IOException ex) {
            return new JsonRepresentation( "ERRO: " + ex.getMessage());
        }
    }

    @Put
    public Representation baixaDeEstoque(Representation rep) {
        try {
            this.clientResourceProduto = new ClientResource("http://localhost:9099/app/baixa");
            Representation resp = this.clientResourceProduto.put(rep);
            return new JsonRepresentation( resp.getText() );
        } catch (IOException ex) {
            return new JsonRepresentation( "ERRO: " + ex.getMessage());
        }

    }
}
