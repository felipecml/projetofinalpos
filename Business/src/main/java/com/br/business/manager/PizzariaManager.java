/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.business.manager;

import com.br.business.caixa.CaixaBusiness;
import com.br.business.execoes.CaixaException;
import com.br.business.execoes.ComandaException;
import com.br.business.execoes.PizzariaException;
import com.br.business.execoes.ProdutoException;
import com.br.business.pedido.PedidoBusiness;
import com.br.business.produto.ProdutoBusiness;
import com.br.core.model.Comanda;
import com.br.core.model.Estoque;
import com.br.core.model.ItemPedido;
import com.br.core.model.LancamentoCaixa;
import com.br.core.model.Produto;
import java.io.Serializable;

/**
 *
 * @author Felipe
 */
public class PizzariaManager implements Serializable {

    private PedidoBusiness pedidoBusiness;
    private CaixaBusiness caixaBusiness;
    private ProdutoBusiness produtoBusiness;

    public PizzariaManager() {
        this.pedidoBusiness = new PedidoBusiness();
        this.caixaBusiness = new CaixaBusiness();
        this.produtoBusiness = new ProdutoBusiness();
    }

    //fazer pedido
    public boolean realizarPedido(String codMesa, String descricaoProduto, Integer qtde) throws PizzariaException {

        Estoque e = (Estoque) this.produtoBusiness.consultarEstoquePorProduto(descricaoProduto);

        if (e != null) {
            if (e.getTotal() < qtde) {
                try {
                    Comanda c = (Comanda) this.pedidoBusiness.recuperarComanda(codMesa);
                    c.getPedidos().add(new ItemPedido(e.getProduto(), qtde, false));
                    this.pedidoBusiness.atualizarComanda(c);
                    this.produtoBusiness.atualizarSaidaEstoque(e, qtde);
                    return true;
                } catch (ComandaException ex) {
                    throw new PizzariaException("Erro na comanda: " + ex.getMessage());
                } catch (ProdutoException ex) {
                    throw new PizzariaException("Erro na atualização do estoque: " + ex.getMessage());
                } 
            }
            throw new PizzariaException("O produto está em falta.");
        } else {
            throw new PizzariaException("Produto não está cadastrado.");
        }
    }

    //entregar pedido    
    public boolean entregarPedido(String codMesa, String descricaoProduto) throws PizzariaException {

        Produto p = (Produto) this.produtoBusiness.buscarProduto(descricaoProduto);

//        if (p != null) {
//            try {
//                this.pedidoBusiness.entregarPedido(codMesa, p);
//            } catch (ComandaException ex) {
//                throw new PizzariaException("Erro na comanda: " + ex.getMessage());
//            }
//        }

        return true;
    }

    //pagar
    public boolean pagarComanda(String codMesa) throws PizzariaException {
        try {
            Comanda c = this.pedidoBusiness.recuperarComanda(codMesa);
            LancamentoCaixa lancamentoCaixa = new LancamentoCaixa(c.calcular(), c);
            this.caixaBusiness.lancamento(lancamentoCaixa);
            this.pedidoBusiness.fecharComanda(c);
            return true;
        } catch (ComandaException | CaixaException ex) {
            throw new PizzariaException("Erro de comanda: " + ex.getMessage());
        }
    }
    
    public boolean baixaNoEstoque(String descricao, int total)throws ProdutoException{
        
        if(total < 0){
            throw new ProdutoException("A quantidade debitada não pode ser negativa.");
        }
        
        Estoque e = (Estoque) this.produtoBusiness.consultarEstoquePorProduto(descricao);
        
        if(e.getTotal() < total){
            throw new ProdutoException("Quantidade superior ao estoque. ESTOQUE: " + e.getTotal());
        }
        
        return this.produtoBusiness.atualizarSaidaEstoque(e, total);
    }
    
}
