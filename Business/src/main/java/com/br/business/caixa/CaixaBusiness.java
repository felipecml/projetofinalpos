/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.business.caixa;

import com.br.business.dao.DAO;
import com.br.business.execoes.CaixaException;
import com.br.business.execoes.DAOException;
import com.br.core.model.LancamentoCaixa;
import java.io.Serializable;

/**
 *
 * @author Felipe
 */
public class CaixaBusiness implements Serializable{
    
    private DAO dao;
    
    public CaixaBusiness(){
        this.dao = new DAO();
    }
    
    public boolean lancamento(LancamentoCaixa lancamentoCaixa)throws CaixaException{
        try {
            return this.dao.persist(lancamentoCaixa);
        } catch (DAOException ex) {
            ex.printStackTrace();
            throw new CaixaException("Não foi possivel realizar este lancamento. Contate o administrador do sistema.");
        }
    }
    
}
