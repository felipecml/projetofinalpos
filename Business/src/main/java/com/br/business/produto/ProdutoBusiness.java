/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.business.produto;

import com.br.business.dao.DAO;
import com.br.business.execoes.DAOException;
import com.br.business.execoes.ProdutoException;
import com.br.core.model.Estoque;
import com.br.core.model.Produto;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Felipe
 */
public class ProdutoBusiness implements Serializable{
    
    private DAO dao;
    
    public ProdutoBusiness(){
        this.dao = new DAO();
    }
    
    public boolean cadastrarProduto(Produto produto, int qtde)throws ProdutoException{
        try {
            Estoque estoque = new Estoque(qtde, produto);
            return this.dao.persist(estoque);
        } catch (DAOException ex) {
            throw new ProdutoException("Erro de cadastro produto: "+ex.getMessage());
        }
    }
    
    public Produto buscarProduto(String descricao){
        Map<String, Object> parametro = new HashMap<>();
        parametro.put("descricao", descricao);
        Produto p = (Produto) this.dao.findBy("buscar.produto.por.descricao", parametro);
        return p;
    }
    
    public Produto consultarProdutoEstoque(String descricao){
        Map<String, Object> parametro = new HashMap<>();
        parametro.put("descricao", descricao);
        Produto p = (Produto) this.dao.findBy("buscar.produto.em.estoque.por.descricao", parametro);
        return p;
    }
    
    public Estoque consultarEstoquePorProduto(String descricao){
        Map<String, Object> parametro = new HashMap<>();
        parametro.put("descricao", descricao);
        Estoque e = (Estoque) this.dao.findBy("buscar.estoque.por.descricao.produto", parametro);
        return e;
    }
    
    public boolean atualizarSaidaEstoque(Estoque e, int qtde)throws ProdutoException{
        try {
            int valorAtual = e.getTotal() - qtde;
            
            Map<String, Object> parametro = new HashMap<>();
            parametro.put("idEstoque", e.getId());
            parametro.put("total", valorAtual);
            
            return this.dao.execute("baixa.estoque", parametro);
        } catch (DAOException ex) {
            throw new ProdutoException("Não foi possivel atualizar o estoque. Erro: " + ex.getMessage());
        }
    }

    public boolean atualizar(Estoque e){
        return this.dao.update(e);
    }
    
}
