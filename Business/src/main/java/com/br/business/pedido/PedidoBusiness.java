/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.business.pedido;

import com.br.business.dao.DAO;
import com.br.business.execoes.ComandaException;
import com.br.business.execoes.DAOException;
import com.br.core.model.Comanda;
import com.br.core.model.ItemPedido;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Felipe
 */
public class PedidoBusiness implements Serializable {

    private DAO dao;

    public PedidoBusiness() {
        this.dao = new DAO();
    }

    //abrir comanda
    public boolean abrirComanda(String codMesa, String codComanda) throws ComandaException{
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("codComanda", codComanda);
        Comanda c = (Comanda) this.dao.findBy("buscar.comanda.por.codigo.comanda", parametros);

        if (c == null) {
            try {
                return this.dao.persist(new Comanda(codComanda, codMesa, true));
            } catch (DAOException ex) {
                throw new ComandaException("Não foi possivel abrir esta comanda. Contate o administrador do sistema.");
            }
        }
        
        throw new ComandaException("Já existe uma comanda aberta para esta mesa.");
        
    }

    //fechar comanda
    public boolean fecharComanda(Comanda c) throws ComandaException{

        if (c != null) {
            c.setAberta(false);
            return this.dao.update(c);
        }
        
        throw new ComandaException("Esta mesa não possui comanda.");
    }

    //recuperar comanda
    public Comanda recuperarComanda(String codComanda) throws ComandaException{

        Map<String, Object> parametros = new HashMap<>();
        parametros.put("codComanda", codComanda);
        Comanda c = (Comanda) this.dao.findBy("buscar.comanda.por.codigo.comanda", parametros);

        if (c != null) {
            return c;
        }

        throw new ComandaException("Esta mesa não possui comanda.");

    }
    
    //recuperar comanda
    public ItemPedido recuperarItem(Long idPedido) throws ComandaException{

        Map<String, Object> parametros = new HashMap<>();
        parametros.put("idPedido", idPedido);
        ItemPedido i = (ItemPedido) this.dao.findBy("buscar.item.pelo.codigo", parametros);

        if (i != null) {
            return i;
        }

        throw new ComandaException("Não há itens pedidos.");

    }

    //update comanda
    public boolean atualizarComanda(Comanda c){
        return this.dao.update(c);
    }
    
    //fazer pedido
//    public boolean fazerPedido(String codMesa, Produto produto, Integer qtde) throws ComandaException{
//
//        Map<String, Object> parametros = new HashMap<>();
//        parametros.put("codMesa", codMesa);
//        Comanda c = (Comanda) this.dao.findBy("buscar.comanda.por.codigo.mesa", parametros);
//
//        if (c != null) {
//            ItemPedido itemPedido = new ItemPedido(produto, qtde, false);
//            c.getPedidos().add(itemPedido);
//            return this.dao.update(c);
//        }
//
//        throw new ComandaException("Esta mesa não possui comanda.");
//    }

    //entregar itens
//    public boolean entregarPedido(String codMesa, Produto produto) throws ComandaException{
//        Map<String, Object> parametros = new HashMap<>();
//        parametros.put("codMesa", codMesa);
//        Comanda c = (Comanda) this.dao.findBy("buscar.comanda.por.codigo.mesa", parametros);
//
//        if (c != null) {
//            for (ItemPedido item : c.getPedidos()) {
//                if (item.getId() == produto.getId()) {
//                    item.setEntregue(true);
//                    this.dao.update(c);
//                    return true;
//                }
//            }
//            throw new ComandaException("O item que está sendo entregue, não pertence a esta comanda.");
//        }
//
//        throw new ComandaException("Esta mesa não possui comanda.");
//    }

    //fechar comanda
}
