/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.business.dao;

import com.br.business.execoes.DAOException;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author Felipe
 */
public class DAO implements Serializable {

    private EntityManager entityManager;

    public DAO() {
        this.entityManager = Persistence.createEntityManagerFactory("persistence").createEntityManager();
    }

    public boolean persist(Object obj) throws DAOException {
        try {
            this.entityManager.getTransaction().begin();
            this.entityManager.persist(obj);
            this.entityManager.getTransaction().commit();
            return true;
        } catch (Exception e) {
            throw new DAOException("Erro de persistencia: " + e.getMessage());
        }
    }

    public Object find(Long id, Class klass) {
        return this.entityManager.find(klass, id);
    }

    public boolean update(Object obj) {
        try {
            this.entityManager.getTransaction().begin();
            this.entityManager.detach(obj);
            this.entityManager.merge(obj);
            this.entityManager.getTransaction().commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public Object findBy(String namedQuery, Map<String, Object> parameter) {
        Query query = this.entityManager.createNamedQuery(namedQuery);
        query = setParameter(query, parameter);
        List list = query.getResultList();
        if (list == null) {
            return null;
        }
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    public List all(String namedQuery) {
        return list(namedQuery, new HashMap<String, Object>());
    }

    public List allBy(String namedQuery, Map<String, Object> parameter) {
        return list(namedQuery, parameter);
    }

    public boolean execute(String namedQuery, Map<String, Object> parameter) throws DAOException{
        try {
            this.entityManager.getTransaction().begin();
            Query query = this.entityManager.createNamedQuery(namedQuery);
            query = setParameter(query, parameter);
            boolean s = query.executeUpdate() > 0;
            this.entityManager.getTransaction().commit();
            return s;
        } catch (Exception e) {
            throw new DAOException("Erro de execução: " + e.getMessage());
        }
    }

    private List<Object> list(String namedQuery, Map<String, Object> parameter) {
        Query query = this.entityManager.createNamedQuery(namedQuery);
        query = setParameter(query, parameter);
        return query.getResultList();
    }

    private Query setParameter(Query q, Map<String, Object> p) {
        for (String str : p.keySet()) {
            if (p.get(str) instanceof Date) {
                q.setParameter(str, (Date) p.get(str), TemporalType.DATE);
            } else {
                q.setParameter(str, p.get(str));
            }
        }
        return q;
    }
}
